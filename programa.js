class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[];
    circulos=[];
    poligonos=[];

    constructor(){

        this.posicionInicial=[4.709143568562426,-74.22394931316376];
        this.escalaInicial=14;
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        this.atributosProveedor={
            maxZoom:20
        };


        this.miVisor=L.map("mapid");
        this.miVisor.setView(this.posicionInicial,this.escalaInicial);
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor);
        this.mapaBase.addTo(this.miVisor);
    }


    colocarMarcador(posicion)
    {
        this.marcadores.push(L.marker(posicion));
        this.marcadores[this.marcadores.length-1].addTo(this.miVisor);
    }

    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion));
        this.circulos[this.circulos.length-1].addTo(this.miVisor);

    }

    colocarPoligono (posicion)
    {
        this.poligonos.push(L.polygon(posicion));
        this.poligonos[this.poligonos.length-1].addTo(this.miVisor);
    }


}

let miMapa=new Mapa();

miMapa.colocarMarcador([4.715548417664286,-74.13606405258179]);
miMapa.colocarMarcador([4.715548417664286,-74.13606405258179]);



miMapa.colocarCirculo([4.715548417664286,-74.13606405258179], {
    color: 'blue',
    fillColor: 'yellow',
    fillOpacity: 0.5,
    radius: 500
});
miMapa.colocarCirculo([4.715548417664286,-74.13606405258179], {
    color: 'blue',
    fillColor: 'blue',
    fillOpacity: 0.5,
    radius: 50
});


miMapa.colocarPoligono([
    [4.718136002726354,-74.14308071136475]
    [4.717922153086764,-74.13844585418701]
    [4.718307082390586,-74.13595676422119]
    [4.715355952274436,-74.13342475891113]
    [4.712575890696034,-74.13398265838623]
    [4.708256080643689,-74.13419723510742]
    [4.705561532074705,-74.13844585418701]
    [4.7121054176362716,-74.14556980133057]
    [4.715099331671699,-74.14320945739746]
    [4.717922153086764,-74.14316654205322]
])
